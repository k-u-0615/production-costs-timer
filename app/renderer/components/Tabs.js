const Vue = require("../vue.min.js")

Vue.component("v-tabs", {
  name: "v-tabs",
  template: /*html*/ `
  <div>
    <div class="tabs is-centered">
      <ul>
        <li v-for="tab of tabs" v-bind:class="{'is-active': tab.isActive}">
          <a @click="onTabClick(tab)" style="font-size: 14px;">
            <span v-if="tab.icon" class="icon"><i :class="['fas', tab.icon]"></i></span>
            <span>{{tab.title}}</span>
          </a>
        </li>
      </ul>
    </div>
    <div class="content" style="margin: 20px;">
      <slot/>
    </div>
  </div>`,
  data: () => ({ tabs: [] }),
  mounted() {
    this.tabs = this.$children
    this.tabs[0].isActive = true
  },
  methods: {
    onTabClick(tab) {
      this.tabs.forEach($el => $el.isActive = false)
      tab.isActive = true
    }
  },
  computed: {
    selected() {
      return this.tabs.find($el => $el.isActive)
    }
  }
})

Vue.component("v-tab", {
  name: "v-tab",
  template: /*html*/`<div v-if="isActive"><slot/></div>`,
  props: ["icon", "title"],
  data: () => ({ isActive: false })
})
